import * as actions from '../actionTypes.js';

const reducer = (state = {content: {}, user: {set: false}}, action) => {
  switch(action.type){
    case actions.setUser:
      return {
        ...state,
        user: {
          set: true,
          username: action.payload.username
        }
      }
    case actions.clearUser:
      return {
        ...state,
        user: {
          set: false
        }
      }
    default:
      return state;
  }
}

export default reducer;