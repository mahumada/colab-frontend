import * as actions from '../../actionTypes.js';

export const set = username => {
  return dispatch => {
    dispatch({
      type: actions.setUser,
      payload: {
        username
      }
    })
  }
}

export const clear = () => {
  return dispatch => {
    dispatch({
      type: actions.clearUser
    })
  }
}