import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './views/Home.js';
import User from './views/UserPage.js';
import Error from './views/Error.js'

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/user">
          <User />
        </Route>
        <Route path="*">
          <Error />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
