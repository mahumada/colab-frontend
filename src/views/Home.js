import { useSelector, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from '../state/index.js';
import React from 'react';
import Navbar from '../components/Navbar.js';
import ButtonSmall from '../components/ButtonSmall.js';

const Home = () => {

  document.title = 'Colab - Home';

  const state = useSelector(state => state);
  console.log(state.user);

  const dispatch = useDispatch();
  const actions = bindActionCreators(actionCreators, dispatch);

  const sayHey = () => {
    console.log('hello');
    return;
  }

  return (
    <>
      <Navbar current='home'/>
      <ButtonSmall text="Say Hello!" onClick={() => sayHey}/>
    </>
  )
}

export default Home;