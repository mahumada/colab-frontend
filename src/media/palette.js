export const main = '#5ACDB1';
export const alternative = '#426A5A';
export const dark = '#1E1E24';
export const light = '#7F7F7F';
export const lighter = '#FFFDFD';