import React from 'react'
import styled from 'styled-components';
import * as colors from '../media/palette.js';

const Wrapper = styled.nav`
  height: 10vh;
  width: 100vw;
  background: ${colors.main};
  color: ${colors.dark};
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
`

const Image = styled.img`
  border-radius: 2vh;
  height: 8vh;
  width: 25vh;
  text-align: center;
  border: .3vh solid ${colors.lighter};
  background: ${colors.alternative};
`

const Links = styled.ul`
  list-style: none;
  width: 50vw;
  height: auto;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
`

const Link = styled.li`

`

const Redirect = styled.a`
  font-size: 3.5vmin;
  color: ${colors.lighter};
  text-decoration: none;
  transition: 1s;

  &:hover {
    font-size: 4vmin;
    text-shadow: .3vmin .3vmin .8vmin black;
  }
`

const Navbar = () => {
  return (
    <Wrapper>
      <Image src="logo.png" alt="Logo"/>
      <Links>
        <Link>
          <Redirect href="/">Home</Redirect>
        </Link>
        <Link>
          <Redirect href="/user">User</Redirect>
        </Link>
        <Link>
          <Redirect href="/tracks">Tracks</Redirect>
        </Link>
        <Link>
          <Redirect href="/playlists">Playlists</Redirect>
        </Link>
      </Links>
    </Wrapper>
  )
}

export default Navbar;
