import React from 'react';
import * as colors from '../media/palette.js';
import styled from 'styled-components';

const Button = styled.button`
  height: auto;
  font-size: 2vmin;
  background: ${colors.main};
  border: .4vmin solid ${colors.lighter};
  border-radius: 1vmin;
  padding: .8vmin;
  transition: 1s;

  &:hover {
    background: ${colors.lighter};
    border-radius: .7vmin;
    box-shadow: .4vmin .4vmin 1.4vmin black;
    transform: scale(1.1);
  }
`

const ButtonSmall = ({onClick, text}) => {
  return (
    <Button onClick={onClick()}>
      {text}
    </Button>
  )
}

export default ButtonSmall;